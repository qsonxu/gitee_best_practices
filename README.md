### 活动介绍
在我们使用码云进行开发的过程中，经常会get到一些最佳的解决方法，这种方式通常比其他解决方式都更有效，如今越来越多的企业和开发者选择码云，为了更好地展示这些优秀企业和个人开发者的应用，同时为其他企业及开发者提供码云最佳实践上的参考，我们特别举办了此次征文活动。快来秀一秀你使用码云管理代码的神操作吧！
  
   
### 参与方式

任何人都可以提交作品，可通过[轻量级PR](https://gitee.com/help/articles/4291)：在本仓库内创建文件，提交 Pull Requests 来提交参赛作品。
作品规则：  
参与活动的作品可以是码云社区版，码云高校版，码云企业版的任何一个版块；  
请给文件取一个有意义的文件名，文件名长度不得超过 20 个字符（不含扩展名），不能和他人的文件重名；  
允许提交多个作品；  

### 投稿奖励（二选一）
- [Gitee 巨型鼠标垫](https://item.taobao.com/item.htm?spm=a1z10.1-c-s.w4001-21733612070.3.5b47194dWJCxVU&id=597157043586&scene=taobao_shop)  
- [Gitee T恤](https://item.taobao.com/item.htm?spm=a1z10.1-c-s.w4004-21733633067.5.5b47194dWJCxVU&id=597155407543)
 
### 参考作品

[微信小程序如何使用Git实现版本管理和协作开发](https://gitee.com/oschina/gitee_best_practices/blob/master/%E5%BE%AE%E4%BF%A1%E5%B0%8F%E7%A8%8B%E5%BA%8F%E5%A6%82%E4%BD%95%E4%BD%BF%E7%94%A8Git%E5%AE%9E%E7%8E%B0%E7%89%88%E6%9C%AC%E7%AE%A1%E7%90%86%E5%92%8C%E5%8D%8F%E4%BD%9C%E5%BC%80%E5%8F%91.md)  
    
  

【小贴士】

关于活动有任何问题可私信[@小鱼丁](https://gitee.com/xiaoyuding/)或在码云公众号下留言，我们将在一个工作日内给予回复。  

![输入图片说明](https://gitee.com/uploads/images/2019/0506/103550_be508045_1899542.png "150码云公众号二维码.png")
